# logs

Python 3 logging system.
Support:
Write to files and terminal/shell.
Write errors to stderr and files.
Write warnings to stdout and files.
Write in colour.
Customizable line or lines parsing.
Write strings, lists, tuples and dictionaries.
more...